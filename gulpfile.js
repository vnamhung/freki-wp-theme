'use strict';
var gulp = require( 'gulp' ),
    load = require( 'gulp-load-plugins' )( { pattern: '*' } );

load.glob.sync( './gulp/tasks/*.js' ).forEach( function( path ) {
	require( path );
} );

gulp.task( 'default', gulp.series( gulp.parallel( 'sass', 'javascript', 'watch:main', 'bs' ) ) );

gulp.task( 'default:remote', gulp.parallel( 'sass', 'watch:main', 'bs:remote' ) );

gulp.task( 'default:rtl', gulp.series( gulp.parallel( 'sass', 'watch:main:rtl', 'bs' ) ) );

gulp.task( 'default:document', gulp.parallel( 'bs:document' ) );

gulp.task( 'default:childDemo', gulp.series( gulp.parallel( 'sass:childDemo', 'watch:childDemo', 'bs' ) ) );

'use strict';
var gulp      = require( 'gulp' ),
    mainTheme = require( '../paths' ).mainTheme,
    time      = require( 'dateformat' )( new Date(), 'yyyy-mm-dd_HH-MM' ),
    config    = require( '../../src/' + mainTheme + '/vnh/config.json' ).database,
    mysqlDump = require( 'mysqldump' );


gulp.task( 'backup:db', function( done ) {
	mysqlDump( {
		host: config.host,
		user: config.user,
		password: config.password,
		database: config.database,
		dest: './backup/' + config.database + '_' + time + '.sql'
	}, function( err ) {
		// create data.sql file;
	} );

	done();
} );

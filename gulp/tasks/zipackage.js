'use strict';
var gulp  = require( 'gulp' ),
    del   = require( 'del' ),
    paths = require( '../paths' );

// Clean dist folder.
gulp.task( 'clean:dist', function() {
	return del( [ 'dist/**' ] );
} );

// Clean dist after build package.
gulp.task( 'clean:dist:after', function() {
	return del( [ 'dist/**/*', '!dist/*.zip' ] );
} );

// Copy changelog to dist folder
gulp.task( 'copy:changelog', function() {
	return gulp.src( paths.root.main + 'readme.txt', { base: paths.root.main } )
	           .pipe( gulp.dest( 'dist/' ) );
} );

// Package main theme.
gulp.task( 'package:main', gulp.series( 'translate', 'sass:full', 'javascript:full', 'rtl', 'zip:full', 'size' ) );

// Package main theme without scss and zip files.
gulp.task( 'package:mini', gulp.series( 'translate', 'sass:full', 'javascript:full', 'rtl', 'zip:mini', 'size' ) );

// Package.
gulp.task( 'package', gulp.series( 'translate', 'sass:full', 'javascript:full', 'rtl', 'zip:full', 'zip:mini', 'size' ) );

'use strict';
var gulp  = require( 'gulp' ),
    load  = require( 'gulp-load-plugins' )(),
    spawn = require( 'child_process' ).spawn,
    paths = require( '../paths' ).linting;

gulp.task( 'lint:php', function() {
	return gulp.src( paths.phpcs )
	           .pipe( load.phpcs( {
		           bin: paths.bin,
		           standard: 'ruleset.xml',
		           showSniffCode: true
	           } ) )
	           .pipe( load.phpcs.reporter( 'log' ) );
} );

gulp.task( 'lint:scss', function( done ) {
	spawn( 'stylelint', [ paths.scss, '--syntax scss' ], { stdio: 'inherit' } );
	done();
} );

gulp.task( 'lint:js', function( done ) {
	spawn( 'eslint', [ paths.js ], { stdio: 'inherit' } );
	done();
} );

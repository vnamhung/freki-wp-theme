'use strict';
var gulp       = require( 'gulp' ),
    paths      = require( '../paths' ),
    bs         = require( 'browser-sync' ).create( paths.mainTheme ),
    bsReuseTab = require( 'browser-sync-reuse-tab' )( bs ),
    bsConfig   = require( '../bs-config' ),
    proxy      = require( '../../src/' + paths.mainTheme + '/vnh/config.json' ).proxy;

gulp.task( 'bs', function() {
	bs.init( {
		files: paths.code.main,
		ghostMode: bsConfig.ghostMode,
		port: bsConfig.port,
		notify: bsConfig.notify
	} );
} );

gulp.task( 'bs:remote', function() {
	bs.init( {
		proxy: proxy.remote,
		files: paths.code.main,
		ghostMode: bsConfig.ghostMode,
		notify: bsConfig.notify,
		open: false,
		serveStatic: [
			{
				route: '/wp-content/themes/' + paths.mainTheme, // remote path
				dir: paths.root.main  // local path
			}
		]
	}, bsReuseTab );
} );

gulp.task( 'bs:document', function() {
	bs.init( {
		server: paths.root.document,
		notify: bsConfig.notify,
		open: false,
		plugins: [
			{
				module: 'bs-html-injector',
				options: { files: paths.code.document }
			}
		]
	}, bsReuseTab );
} );

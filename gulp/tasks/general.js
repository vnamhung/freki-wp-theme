'use strict';
var gulp  = require( 'gulp' ),
    load  = require( 'gulp-load-plugins' )(),
    del   = require( 'del' ),
    paths = require( '../paths' );

// Check the size of all files in dist folder
gulp.task( 'size', function() {
	return gulp.src( 'dist/**/*.zip' )
	           .pipe( load.size( {
		           pretty: true,
		           showFiles: true
	           } ) )
} );

// Delete all woo file if the theme don't need them
gulp.task( 'del:woo', function() {
	return del( [
		paths.root.main + 'assets/images/icons/**',
		paths.root.main + 'assets/images/photoswipe/**',
		paths.root.main + 'assets/libs/woo-fonts/**',
		paths.root.main + 'assets/js/modules/_minicart.js',
		paths.root.main + 'assets/scss/00.settings/_set.woo.scss',
		paths.root.main + 'assets/scss/07.woocommerce/**',
		paths.root.main + 'assets/scss/08.woocommerce-extra/**',
		paths.root.main + 'vnh/woocommerce/**'
	] );
} );

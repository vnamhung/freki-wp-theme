'use strict';
var gulp = require( 'gulp' ),
    load = require( 'gulp-load-plugins' )(),
    path = require( '../paths' ).root.main;

// Build rtl.css.
gulp.task( 'rtl', function() {
	return gulp.src( path + 'style.css' )
	           .pipe( load.rtlcss() )
	           .pipe( load.rename( 'style-rtl.css' ) )
	           .pipe( gulp.dest( path ) );
} );

'use strict';
var gulp        = require( 'gulp' ),
    load        = require( 'gulp-load-plugins' )(),
    paths       = require( '../paths' ).javascript,
    reportError = require( '../report-bug' );

gulp.task( 'javascript', function() {
	return gulp.src( paths.theme.src )
	           .pipe( load.plumber( { errorHandler: reportError } ) )
	           .pipe( load.sourcemaps.init() )
	           .pipe( load.concat( 'main.js' ) )
	           .pipe( load.sourcemaps.write( '.' ) )
	           .pipe( gulp.dest( paths.theme.dist ) );
} );

gulp.task( 'javascript:framework', function() {
	return gulp.src( paths.framework.src )
	           .pipe( load.plumber( { errorHandler: reportError } ) )
	           .pipe( load.sourcemaps.init() )
	           .pipe( load.concat( 'framework.js' ) )
	           .pipe( load.sourcemaps.write( '.' ) )
	           .pipe( gulp.dest( paths.framework.dist ) );
} );

gulp.task( 'javascript:full', function() {
	return gulp.src( paths.theme.src )
	           .pipe( load.plumber( { errorHandler: reportError } ) )
	           .pipe( load.sourcemaps.init() )
	           .pipe( load.concat( 'main.min.js' ) )
	           .pipe( load.uglify() )
	           .pipe( load.sourcemaps.write( '.' ) )
	           .pipe( gulp.dest( paths.theme.dist ) );
} );

'use strict';
var gulp  = require( 'gulp' ),
    load  = require( 'gulp-load-plugins' )(),
    time  = require( 'dateformat' )( new Date(), 'yyyy-mm-dd_HH-MM' ),
    paths = require( '../paths' ),
    ver   = require( '../../src/' + paths.mainTheme + '/vnh/config.json' ).version;

// Zip main theme.
gulp.task( 'zip:full', function() {
	return gulp.src( paths.zip.full, { base: 'src/' } )
	           .pipe( load.zip( paths.mainTheme + '_' + ver + '_' + time + '.zip' ) )
	           .pipe( gulp.dest( 'dist/themes/' ) );
} );

// Zip main theme without scss and zip files.
gulp.task( 'zip:mini', function() {
	return gulp.src( paths.zipMini, { base: 'src/' } )
	           .pipe( load.zip( paths.mainTheme + '_mini_' + ver + '_' + time + '.zip' ) )
	           .pipe( gulp.dest( 'dist/themes/' ) );
} );

'use strict';
var gulp       = require( 'gulp' ),
    exec       = require( 'child_process' ).exec,
    mainTheme  = require( '../paths' ).mainTheme,
    remote     = require( '../../src/' + mainTheme + '/vnh/config.json' ).deploy.remote,
    remotePath = remote.user + '@' + remote.domain + ':/var/www/' + remote.subDomain + '.' + remote.domain + '/htdocs/',
    data       = 'data/',
    paths      = {
	    local: {
		    data: data
	    },
	    remote: {
		    data: remotePath + data
	    }
    };

gulp.task( 'push:themes', function( done ) {
	var cmd = 'gulp javascript:full && wordmove push -t -e live';
	exec( cmd, function( error, stdio, stderr ) {
		console.log( 's: ' + stdio + '\n' );
		console.log( 'e: ' + stderr + '\n' );
	} );
	done();
} );

gulp.task( 'push:uploads', function( done ) {
	var cmd = 'wordmove push -u -e live';
	exec( cmd, function( error, stdio, stderr ) {
		console.log( 's: ' + stdio + '\n' );
		console.log( 'e: ' + stderr + '\n' );
	} );
	done();
} );

gulp.task( 'push:plugins', function( done ) {
	var cmd = 'wordmove push -p -e live';
	exec( cmd, function( error, stdio, stderr ) {
		console.log( 's: ' + stdio + '\n' );
		console.log( 'e: ' + stderr + '\n' );
	} );
	done();
} );

gulp.task( 'push:data', function( done ) {
	var cmd = 'rsync -avzhe ssh --delete -L ' + paths.local.data + ' ' + paths.remote.data;
	exec( cmd, function( error, stdio, stderr ) {
		console.log( 's: ' + stdio + '\n' );
		console.log( 'e: ' + stderr + '\n' );
	} );
	done();
} );

gulp.task( 'push:db', function( done ) {
	var cmd = 'wordmove push -d -e live';
	exec( cmd, function( error, stdio, stderr ) {
		console.log( 's: ' + stdio + '\n' );
		console.log( 'e: ' + stderr + '\n' );
	} );
	done();
} );

gulp.task( 'pull:uploads', function( done ) {
	var cmd = 'wordmove pull -u -e live';
	exec( cmd, function( error, stdio, stderr ) {
		console.log( 's: ' + stdio + '\n' );
		console.log( 'e: ' + stderr + '\n' );
	} );
	done();
} );

gulp.task( 'pull:plugins', function( done ) {
	var cmd = 'wordmove pull -p -e live';
	exec( cmd, function( error, stdio, stderr ) {
		console.log( 's: ' + stdio + '\n' );
		console.log( 'e: ' + stderr + '\n' );
	} );
	done();
} );

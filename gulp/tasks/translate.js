'use strict';
var gulp   = require( 'gulp' ),
    load   = require( 'gulp-load-plugins' )(),
    paths   = require( '../paths' ),
    config = require( '../../src/' + paths.mainTheme + '/vnh/config.json' ).translate;

// Update Pot file.
gulp.task( 'translate', function() {
	return gulp.src( paths.root.main + '**/*.php' )
	           .pipe( load.sort() )
	           .pipe( load.wpPot( {
		           bugReport: config.bugReport,
		           team: config.team
	           } ) )
	           .pipe( gulp.dest( paths.root.main + '/languages/' + paths.mainTheme + '.pot' ) );
} );

'use strict';
var gulp        = require( 'gulp' ),
    load        = require( 'gulp-load-plugins' )(),
    paths       = require( '../paths' ),
    processors  = require( '../processors' ),
    reportError = require( '../report-bug' );

// Build SASS.
gulp.task( 'sass', function() {
	return gulp.src( paths.sass.main.generate )
	           .pipe( load.plumber( { errorHandler: reportError } ) )
	           .pipe( load.sourcemaps.init( { largeFile: true } ) )
	           .pipe( load.sassGlob() )
	           .pipe( load.sass( { outputStyle: 'compressed' } ) )
	           .pipe( load.postcss( processors.modules ) )
	           .pipe( load.lineEndingCorrector() )
	           .pipe( load.sourcemaps.write( 'assets/scss/' ) )
	           .pipe( gulp.dest( paths.root.main ) );
} );

// Build SASS final.
gulp.task( 'sass:full', function() {
	return gulp.src( paths.sass.main.generate )
	           .pipe( load.plumber( { errorHandler: reportError } ) )
	           .pipe( load.sourcemaps.init( { largeFile: true } ) )
	           .pipe( load.sassGlob() )
	           .pipe( load.sass( { outputStyle: 'compressed' } ) )
	           .pipe( load.postcss( processors.modulesFull ) )
	           .pipe( load.cssnano( processors.nano ) )
	           .pipe( load.lineEndingCorrector() )
	           .pipe( load.sourcemaps.write( 'assets/scss/' ) )
	           .pipe( gulp.dest( paths.root.main ) );
} );

// Build sass for child theme demo.
gulp.task( 'sass:childDemo', function() {
	return gulp.src( paths.sass.childDemo )
	           .pipe( load.plumber( { errorHandler: reportError } ) )
	           .pipe( load.sourcemaps.init( { largeFile: true } ) )
	           .pipe( load.sassGlob() )
	           .pipe( load.sass() )
	           .pipe( load.postcss( processors.modules ) )
	           .pipe( load.lineEndingCorrector() )
	           .pipe( load.sourcemaps.write( 'assets/scss/' ) )
	           .pipe( gulp.dest( paths.root.childDemo ) );
} );

'use strict';
var     mainTheme  = require( './paths' ).mainTheme,
        config = require( '../src/' + mainTheme + '/vnh/config.json' ).bsConfig;

// Config notify for Browser Sync
module.exports = {
	port: config.port,
	ghostMode: config.ghostMode,
	notify: {
		styles: {
			backgroundColor: config.notify.backgroundColor,
			fontSize: config.notify.fontSize,
			top: config.notify.top,
			borderBottomLeftRadius: '0',
			fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif'
		}
	}
};

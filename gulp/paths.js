'use strict';
var glob           = require( 'glob' ),
    files          = glob( 'src/*', { sync: true } ),
    mainTheme      = files[ 0 ].replace( 'src/', '' ),
    childThemeDemo = files[ 1 ].replace( 'src/', '' );

module.exports = {
	mainTheme: mainTheme,
	taskDone: [
		'src/**/*.php',
		'gulp/**/*.js'
	],
	root: {
		main: 'src/' + mainTheme + '/',
		childDemo: 'src/' + childThemeDemo + '/',
		document: 'documentation/'
	},
	javascript: {
		theme: {
			src: 'src/' + mainTheme + '/assets/js/modules/**/*.js',
			dist: 'src/' + mainTheme + '/assets/js/'
		},
		framework: {
			src: 'src/' + mainTheme + '/vnh/framework/assets/js/modules/**/*.js',
			dist: 'src/' + mainTheme + '/vnh/framework/assets/js/'
		}
	},
	sass: {
		main: {
			watch: 'src/' + mainTheme + '/assets/scss/**/*.scss',
			generate: 'src/' + mainTheme + '/assets/scss/*.scss'
		},
		childDemo: 'src/' + childThemeDemo + '/assets/scss/**/*.scss'
	},
	images: [
		'src/' + mainTheme + '/assets/images/*.{JPG,jpg,png,gif}',
		'data/*.{JPG,jpg,png,gif}'
	],
	code: {
		main: [
			'src/' + mainTheme + '/style.css',
			'src/' + mainTheme + '/**/*.php',
			'src/' + mainTheme + '/assets/js/*.js',
			'src/' + mainTheme + '/assets/libs/**/**/*.js'
		],
		childDemo: [
			'src/' + childThemeDemo + '/style.css',
			'src/' + childThemeDemo + '/**/*.php',
			'src/' + childThemeDemo + '/assets/js/*.js'
		],
		document: 'documentation/*.html'
	},
	zip: {
		full: [
			'src/' + mainTheme + '/**/*',
			'!src/' + mainTheme + '/assets/scss/**',
			'!src/' + mainTheme + '/**/tests/**',
			'!src/' + mainTheme + '/**/composer*',
			'!src/' + mainTheme + '/**/Gruntfile.js',
			'!src/' + mainTheme + '/**/phpcs.ruleset.xml',
			'!src/' + mainTheme + '/**/phpunit.xml',
			'!src/' + mainTheme + '/**/update-version.sh',
			'!src/' + mainTheme + '/**/README.md',
			'!src/' + mainTheme + '/**/LICENSE',
			'!src/' + mainTheme + '/**/merlin-config-sample.php',
			'!src/' + mainTheme + '/**/merlin-filters.php',
			'!src/' + mainTheme + '/**/package.json',
			'!src/' + mainTheme + '/**/config.json',
			'!src/' + mainTheme + '/**/dist/*',
			'!src/' + mainTheme + '/**/codemirror/**',
			'!src/' + mainTheme + '/**/demo/*',
			'!src/' + mainTheme + '/**/LICENSE',
			'!src/' + mainTheme + '/**/gulpfile.js',
			'!src/' + mainTheme + '/**/apigen.yml',
			'!src/' + mainTheme + '/**/ruleset.xml',
			'!src/' + mainTheme + '/**/Mobile_Detect.json',
			'!src/' + mainTheme + '/**/examples/**',
			'!src/' + mainTheme + '/**/namespaced/**',
			'!src/' + mainTheme + '/**/export/**',
			'!src/' + mainTheme + '/**/node_modules/**',
			'!src/' + mainTheme + '/**/.*'
		]
	},
	linting: {
		bin: 'vendor/bin/phpcs',
		js: 'src/' + mainTheme + '/assets/js/modules/*.js',
		scss: 'src/' + mainTheme + '/assets/scss/**/*.scss',
		phpcs: [
			'**/*.php',
			'!**/vendor/**',
			'!**/bower_components/**',
			'!**/node_modules/**',
			'!**/modules/**',
			'!**/composer-setup.php'
		]
	}
};

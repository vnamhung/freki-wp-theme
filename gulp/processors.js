'use strict';
var mqpacker       = require( 'css-mqpacker' ),
    autoprefixer   = require( 'autoprefixer' ),
    assets         = require( 'postcss-assets' ),
    verticalRhythm = require( 'postcss-lh' ),
    pxtorem        = require( 'postcss-pxtorem' ),
    path           = require( './paths' ).root.main;

// Config PostCSS modules.
module.exports = {
	nano: {
		autoprefixer: false,
		zindex: false,
		reduceIdents: false,
		discardUnused: false,
		mergeIdents: false
	},
	modules: [
		verticalRhythm()
	],
	modulesFull: [
		autoprefixer( { browsers: [ 'last 4 versions' ] } ),

		verticalRhythm(),

		mqpacker( { sort: true } ),

		assets( { loadPaths: [ path + 'assets/images/', 'data/' ] } ),

		pxtorem( {
			rootValue: 16,
			unitPrecision: 5,
			propWhiteList: [],
			selectorBlackList: [],
			replace: true,
			mediaQuery: false,
			minPixelValue: 2
		} )
	]
};

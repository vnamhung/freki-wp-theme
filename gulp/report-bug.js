'use strict';
var load      = require( 'gulp-load-plugins' )(),
    mainTheme = require( './paths' ).mainTheme;

module.exports = function( error ) {
	load.notify( {
		title: mainTheme + ' | ' + error.plugin,
		subtitle: 'Failed!',
		message: 'See console for more info.',
		sound: true
	} ).write( error );

	load.util.log( load.util.colors.red( error.message ) );

	// Prevent the 'watch' task from stopping
	this.emit( 'end' );
};

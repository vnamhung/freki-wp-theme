<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 */
?>
<footer id="colophon" class="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="footer__wrapper">
		<?php
		/* translators: %s: WordPress */
		printf( esc_html__( 'Proudly powered by %s', 'vnh' ), 'WordPress' );
		?>
	</div><!-- .footer__wrapper -->
</footer><!-- .footer -->



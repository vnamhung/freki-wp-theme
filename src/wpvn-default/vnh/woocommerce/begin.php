<?php

namespace VNH\Woocommerce;

class Begin {
	public function __construct() {
		add_filter( 'woocommerce_enqueue_styles', '__return_false' );

		add_action( 'after_switch_theme', [ $this, 'woocommerce_image_dimensions' ] );

		$this->remove_upsells();
	}

	public function woocommerce_image_dimensions() {
		global $pagenow;

		if ( ! isset( $_GET['activated'] ) || $pagenow !== 'themes.php' ) {
			return;
		}
		$catalog   = array(
			'width'  => '600',
			'height' => '0',
			'crop'   => 0,
		);
		$single    = array(
			'width'  => '600',
			'height' => '600',
			'crop'   => 1,
		);
		$thumbnail = array(
			'width'  => '120',
			'height' => '120',
			'crop'   => 1,
		);
		// Image sizes
		update_option( 'shop_catalog_image_size', $catalog );        // Product category thumbs
		update_option( 'shop_single_image_size', $single );          // Single product image
		update_option( 'shop_thumbnail_image_size', $thumbnail );    // Image gallery thumbs
	}

	public function remove_upsells() {
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
	}
}

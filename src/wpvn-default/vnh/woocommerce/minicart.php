<?php

namespace VNH\Woocommerce;

class Minicart {
	static function cart_count() {
		$qty = WC()->cart->get_cart_contents_count();
		printf( "<a href='#' class='minicart__button'><span>%s</span></a>", esc_html( $qty ) );
	}

	public function add_to_cart_fragment( $fragments ) {
		ob_start();
		$this->cart_count();
		$fragments['.minicart__button'] = ob_get_clean();

		return $fragments;
	}

	public function __construct() {
		add_filter( 'woocommerce_add_to_cart_fragments', [ $this, 'add_to_cart_fragment' ] );
	}
}

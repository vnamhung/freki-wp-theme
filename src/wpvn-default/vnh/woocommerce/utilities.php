<?php

namespace VNH\Woocommerce;

class Utilities {
	public function __construct() {
		// Orderby date.
		add_filter( 'woocommerce_get_catalog_ordering_args', [ $this, 'orderby_date' ] );

		// Disable Woocommerce cart fragments on home page.
		add_action( 'wp_enqueue_scripts', [ $this, 'dequeue_woocommerce_cart_fragments' ], 11 );

		// Notify admin when a new customer account is created.
		add_action( 'woocommerce_created_customer', [ $this, 'created_customer_admin_notification' ] );

		// Hide shipping rates when free shipping is available.
		add_filter( 'woocommerce_package_rates', [ $this, 'hide_shipping_when_free_is_available' ], 100 );

		// Change number of displayed upsells on product pages.
		add_filter( 'woocommerce_upsell_display_args', [ $this, 'upsell_display' ] );

		// Remove subtotal row.
		add_filter( 'woocommerce_get_order_item_totals', [ $this, 'adjust_woocommerce_get_order_item_totals' ] );
	}

	public function dequeue_woocommerce_cart_fragments() {
		if ( is_front_page() && class_exists( 'WooCommerce' ) ) {
			wp_dequeue_script( 'wc-cart-fragments' );
		}
	}

	public function orderby_price_highest_lowest( $args ) {
		$args['meta_key'] = '_price';
		$args['orderby']  = 'meta_value_num';
		$args['order']    = 'desc';

		return $args;
	}

	public function orderby_date( $args ) {
		$args['orderby'] = 'date';
		$args['order']   = 'desc';

		return $args;
	}

	public function orderby_sku( $args ) {
		$args['meta_key'] = '_sku';
		$args['orderby']  = 'meta_value_num';
		$args['order']    = 'asc';

		return $args;
	}

	public function created_customer_admin_notification( $customer_id ) {
		wp_send_new_user_notifications( $customer_id, 'admin' );
	}

	public function hide_shipping_when_free_is_available( $rates ) {
		$free = array();
		foreach ( $rates as $rate_id => $rate ) {
			if ( 'free_shipping' === $rate->method_id ) {
				$free[ $rate_id ] = $rate;
				break;
			}
		}

		return ! empty( $free ) ? $free : $rates;
	}

	public function upsell_display( $args ) {
		$args['posts_per_page'] = 5; // Change this number
		$args['columns']        = 5; // This is the number shown per row.

		return $args;
	}

	public function adjust_woocommerce_get_order_item_totals( $totals ) {
		unset( $totals['cart_subtotal'] );

		return $totals;
	}
}

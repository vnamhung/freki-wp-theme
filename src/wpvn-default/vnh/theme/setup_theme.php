<?php

namespace VNH\Theme;

class Setup_Theme {
	public $content_width = 640;

	public $post_formats = array( // fixit: change them base on theme requirement
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	);

	public $custom_background = array(
		'default-color' => '#ffffff',
		'default-image' => '',
	);

	public function __construct() {
		add_action( 'after_setup_theme', [ $this, 'theme_support' ] );

		add_action( 'after_setup_theme', [ $this, 'content_width' ], 0 );

		add_action( 'after_setup_theme', [ $this, 'other_setup' ] );

//		add_action( 'after_setup_theme', [ $this, 'woocommerce_support' ] );
	}

	public function content_width() {
		$GLOBALS['content_width'] = $this->content_width;
	}

	public function theme_support() {
		/*
		 * Add default posts and comments RSS feed links to head.
		 */
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Support custom Logo.
		 */
		add_theme_support( 'custom-logo' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Support selective refresh for widget
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Enable support for Post Formats.
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support( 'post-formats', $this->post_formats );

		/*
		 * Set up the WordPress core custom background feature.
		 */
		add_theme_support( 'custom-background', $this->custom_background );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );
	}

	public function woocommerce_support() {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

	public function other_setup() {
		// Support editor style.
		add_editor_style( [ 'editor-style.css', fonts_url() ] );

		// Allow the Excerpt to handle shortcodes
		add_filter( 'the_excerpt', 'shortcode_unautop' );
		add_filter( 'the_excerpt', 'do_shortcode' );
	}
}

<?php

namespace VNH\Theme;

use VNH\Framework\Enqueue_Base;

class Enqueue extends Enqueue_Base {
	public $styles  = array();
	public $scripts = array();

	const VERSION = PARENT_THEME_VERSION;

	public function __construct() {
		$this->styles = array(
			'gg-fonts'     => array( // don't change this index name because this also used in resource_hints public function
				'src'     => fonts_url(), //fixit: use it if we don't use kirki
				'deps'    => false,
				'version' => self::VERSION,
				'media'   => 'all',
				'has_rtl' => false,
			),
			'select2'      => array(
				'src'     => self::lib_url( 'select2/css/select2.min.css' ),
				'deps'    => false,
				'version' => '4.0.3',
				'media'   => 'all',
				'has_rtl' => false,
			),
			'font-awesome' => array(
				'src'     => self::lib_url( 'font-awesome/css/font-awesome.min.css' ),
				'deps'    => false,
				'version' => '4.6.3',
				'media'   => 'all',
				'has_rtl' => false,
			),
			'fancybox'     => array(
				'src'     => self::lib_url( 'fancybox/jquery.fancybox.min.css' ),
				'deps'    => false,
				'version' => '3.1.2',
				'media'   => 'all',
				'has_rtl' => false,
			),
			'owl-carousel' => array(
				'src'     => self::lib_url( 'owl.carousel/assets/owl.carousel.css' ),
				'deps'    => false,
				'version' => '2.1.6',
				'media'   => 'all',
				'has_rtl' => false,
			),
			'owl-theme'    => array(
				'src'     => self::lib_url( 'owl.carousel/assets/owl.theme.green.min.css' ),
				'deps'    => false,
				'version' => '2.1.6',
				'media'   => 'all',
				'has_rtl' => false,
			),
			'main'         => array(
				'src'     => get_stylesheet_uri(),
				'deps'    => false,
				'version' => self::VERSION,
				'media'   => 'all',
				'has_rtl' => true,
			),
		);

		$this->scripts = array(
			'select2'         => array(
				'src'       => self::lib_url( 'select2/js/select2.js' ),
				'deps'      => array( 'jquery' ),
				'version'   => '4.0.3',
				'in_footer' => true,
				'have_min'  => true,
				'params'    => null,
			),
			'infinite-scroll' => array(
				'src'       => self::lib_url( 'infinite-scroll/infinite-scroll.pkgd.js' ),
				'deps'      => array( 'jquery' ),
				'version'   => '3.0.0',
				'in_footer' => true,
				'have_min'  => true,
				'params'    => null,
			),
			'owl-carousel'    => array(
				'src'       => self::lib_url( 'owl.carousel/owl.carousel.js' ),
				'deps'      => array( 'jquery' ),
				'version'   => '2.1.6',
				'in_footer' => true,
				'have_min'  => true,
				'params'    => null,
			),
			'fancybox'        => array(
				'src'       => self::lib_url( 'fancybox/jquery.fancybox.js' ),
				'deps'      => array( 'jquery' ),
				'version'   => '3.1.2',
				'in_footer' => true,
				'have_min'  => true,
				'params'    => null,
			),
			'main'            => array(
				'src'       => esc_url( get_theme_file_uri( 'assets/js/main.js' ) ),
				'deps'      => array( 'jquery', 'masonry' ),
				'version'   => self::VERSION,
				'in_footer' => true,
				'have_min'  => true,
				'params'    => array(
					'is_shop' => class_exists( 'WooCommerce' ) ? is_shop() : 0,
				),
			),
		);

		add_action( 'wp_enqueue_scripts', [ $this, 'load_styles_and_scripts' ], 11 );
	}

	public function load_styles_and_scripts() {
		parent::register_styles();
		parent::register_scripts();

		wp_enqueue_style( 'main' );
		wp_enqueue_style( 'fancybox' );
		wp_enqueue_style( 'select2' );
		wp_enqueue_style( 'owl-carousel' );

		wp_enqueue_script( 'fancybox' );
		wp_enqueue_script( 'select2' );
		wp_enqueue_script( 'main' );

		parent::enqueue_framework_script();
		parent::enqueue_comment_reply();
	}

	protected static function lib_url( $dir ) {
		return esc_url( get_theme_file_uri( LIBS_DIR . $dir ) );
	}
}

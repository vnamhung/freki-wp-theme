<?php

namespace VNH\Theme;

use const VNH\FRAMEWORK\MODULES_DIR;

class Setup_Merlin {
	public $config = array();

	public $strings = array();

	public $merlin_url = 'merlin';

	public $demo_directory = SAMPLE_DIR;

	public $support_link = 'https://wordpress.org/support/';

	public $contact_link = 'https://thememove.com/contact/';

	public $child_theme_url = 'https://codex.wordpress.org/Child_Themes';

	public function __construct() {
		add_action( 'after_setup_theme', [ $this, 'setup_merlin' ], 99 );
	}

	public function setup_merlin() {
		$this->config = array(
			'directory'            => untrailingslashit( MODULES_DIR ),
			'demo_directory'       => $this->demo_directory,
			'merlin_url'           => $this->merlin_url,
			'child_action_btn_url' => $this->child_theme_url,
			'help_mode'            => false,
			'branding'             => false,
		);

		$this->strings = array(
			'admin-menu'    => esc_html__( 'Theme Setup', 'vnh' ),
			'title%s%s%s%s' => esc_html__( '%s%s Themes &lsaquo; Theme Setup: %s%s', 'vnh' ),

			'return-to-dashboard' => esc_html__( 'Return to the dashboard', 'vnh' ),

			'btn-skip'             => esc_html__( 'Skip', 'vnh' ),
			'btn-next'             => esc_html__( 'Next', 'vnh' ),
			'btn-start'            => esc_html__( 'Start', 'vnh' ),
			'btn-no'               => esc_html__( 'Cancel', 'vnh' ),
			'btn-plugins-install'  => esc_html__( 'Install', 'vnh' ),
			'btn-child-install'    => esc_html__( 'Install', 'vnh' ),
			'btn-content-install'  => esc_html__( 'Install', 'vnh' ),
			'btn-import'           => esc_html__( 'Import', 'vnh' ),
			'btn-license-activate' => esc_html__( 'Activate', 'vnh' ),

			'welcome-header%s'         => esc_html__( 'Welcome to %s', 'vnh' ),
			'welcome-header-success%s' => esc_html__( 'Hi. Welcome back', 'vnh' ),
			'welcome%s'                => esc_html__( 'This wizard will set up your theme, install plugins, and import content. It is optional & should take only a few minutes.', 'vnh' ),
			'welcome-success%s'        => esc_html__( 'You may have already run this theme setup wizard. If you would like to proceed anyway, click on the "Start" button below.', 'vnh' ),

			'child-header'         => esc_html__( 'Install Child Theme', 'vnh' ),
			'child-header-success' => esc_html__( 'You\'re good to go!', 'vnh' ),
			'child'                => esc_html__( 'Let’s build & activate a child theme so you may easily make theme changes.', 'vnh' ),
			'child-success%s'      => esc_html__( 'Your child theme has already been installed and is now activated — if it wasn\'t already.', 'vnh' ),
			'child-action-link'    => esc_html__( 'Learn about child themes', 'vnh' ),
			'child-json-success%s' => esc_html__( 'Awesome. Your child theme has already been installed and is now activated.', 'vnh' ),
			'child-json-already%s' => esc_html__( 'Awesome. Your child theme has been created and is now activated.', 'vnh' ),

			'plugins-header'         => esc_html__( 'Install Plugins', 'vnh' ),
			'plugins-header-success' => esc_html__( 'You\'re up to speed!', 'vnh' ),
			'plugins'                => esc_html__( 'Let’s install some essential WordPress plugins to get your site up to speed.', 'vnh' ),
			'plugins-success%s'      => esc_html__( 'The required WordPress plugins are all installed and up to date. Press "Next" to continue the setup wizard.', 'vnh' ),
			'plugins-action-link'    => esc_html__( 'Advanced', 'vnh' ),

			'import-header'      => esc_html__( 'Import Content', 'vnh' ),
			'import'             => esc_html__( 'Let’s import content to your website, to help you get familiar with the theme.', 'vnh' ),
			'import-action-link' => esc_html__( 'Advanced', 'vnh' ),

			'license-header%s'    => esc_html__( 'Activate %s', 'vnh' ),
			'license'             => esc_html__( 'Add your license key to activate one-click updates and theme support.', 'vnh' ),
			'license-action-link' => esc_html__( 'More info', 'vnh' ),

			'license-link-1' => wp_kses_post( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', $this->support_link, esc_html__( 'Explore WordPress', 'vnh' ) ) ),
			'license-link-2' => wp_kses_post( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', $this->contact_link, esc_html__( 'Get Theme Support', 'vnh' ) ) ),
			'license-link-3' => wp_kses_post( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', admin_url( 'customize.php' ), esc_html__( 'Start Customizing', 'vnh' ) ) ),

			'ready-header'      => esc_html__( 'All done. Have fun!', 'vnh' ),
			'ready%s'           => esc_html__( 'Your theme has been all set up. Enjoy your new theme by %s.', 'vnh' ),
			'ready-action-link' => esc_html__( 'Extras', 'vnh' ),
			'ready-big-button'  => esc_html__( 'View your website', 'vnh' ),

			'ready-link-1' => wp_kses_post( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', $this->support_link, esc_html__( 'Explore WordPress', 'vnh' ) ) ),
			'ready-link-2' => wp_kses_post( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', $this->contact_link, esc_html__( 'Get Theme Support', 'vnh' ) ) ),
			'ready-link-3' => wp_kses_post( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', admin_url( 'customize.php' ), esc_html__( 'Start Customizing', 'vnh' ) ) ),
		);

		if ( class_exists( '\Merlin' ) ) {
			new \Merlin( $this->config, $this->strings );
		}

		add_filter( 'merlin_generate_child_functions_php', [ $this, 'generate_child_functions_php' ], 10, 2 );
	}

	public function generate_child_functions_php( $output, $slug ) {

		$slug_no_hyphens = strtolower( preg_replace( '#[^a-zA-Z]#', '', $slug ) );

		$output = "
		<?php
		/**
		 * Theme functions and definitions.
		 * This child theme was generated by Merlin WP.
		 *
		 * @link https://developer.wordpress.org/themes/basics/theme-functions/
		 *
		 * If your child theme has more than one .css file (eg. ie.css, style.css, main.css) then 
		 * you will have to make sure to maintain all of the parent theme dependencies.
		 *
		 * Make sure you're using the correct handle for loading the parent theme's styles.
		 * Failure to use the proper tag will result in a CSS file needlessly being loaded twice.
		 * This will usually not affect the site appearance, but it's inefficient and extends your page's loading time.
		 *
		 * @link https://codex.wordpress.org/Child_Themes
		 */

		function {$slug_no_hyphens}_child_enqueue_styles() {
		
			wp_enqueue_style( 'main', esc_url( get_parent_theme_file_uri( 'style.css' ) ) );
		    wp_enqueue_style( '{$slug}-child-style',
		        get_stylesheet_directory_uri() . '/style.css',
		        array( '{$slug}-style' ),
		        wp_get_theme()->get('Version')
		    );
		}

		add_action(  'wp_enqueue_scripts', '{$slug_no_hyphens}_child_enqueue_styles' );\n
	";

		// Let's remove the tabs so that it displays nicely.
		$output = trim( preg_replace( '/\t+/', '', $output ) );

		// Filterable return.
		return $output;
	}
}

<?php

namespace VNH\Theme;

use VNH\Framework\Customize_Base;

class Customize extends Customize_Base {
	public $output_kirki_css = false;

	public $live_edit_title_desc = true;

	public function __construct() {
		parent::__construct();

		add_action( 'widgets_init', [ $this, 'load_customizer' ], 99 );

		add_action( 'customize_register', [ $this, 'remove_customizer_sections' ] );

		add_action( 'customize_preview_init', [ $this, 'customize_preview_js' ] );

		add_filter( 'kirki/fonts/standard_fonts', [ $this, 'standard_fonts' ] );
	}

	public function customize_preview_js() {
		wp_enqueue_script( 'vnh-customize-preview', get_theme_file_uri( THEME_ASSETS_DIR . 'js/customize-preview.js' ), array( 'customize-preview' ), PARENT_THEME_VERSION, true );
	}

	public function load_customizer() {
		require_files( CUSTOMIZER_DIR . 'section*.php' );
	}

	/**
	 * @param \WP_Customize_Manager $wp_customize
	 */
	public function remove_customizer_sections( $wp_customize ) {
		$wp_customize->remove_section( 'nav' );
		$wp_customize->remove_section( 'background_image' );
		$wp_customize->remove_section( 'header_image' );
		$wp_customize->remove_control( 'blogdescription' );
		$wp_customize->remove_control( 'display_header_text' );
	}

	public function standard_fonts( $fonts = array() ) {
		$fonts['helvetica_neue'] = array(
			'label'    => 'Helvetica Neue',
			'variants' => array( 'regular', 'italic', '700', '700italic' ),
			'stack'    => '"Helvetica Neue", Helvetica, Arial, sans-serif',
		);

		$fonts['helvetica'] = array(
			'label'    => 'Helvetica',
			'variants' => array( 'regular', 'italic', '700', '700italic' ),
			'stack'    => 'Helvetica, Arial, sans-serif',
		);

		$fonts['arial'] = array(
			'label'    => 'Arial',
			'variants' => array( 'regular', 'italic', '700', '700italic' ),
			'stack'    => 'Arial, Helvetica, sans-serif',
		);

		return $fonts;
	}

	public static function preset( $preset = array() ) {
		$preset['header'] = array(
			'1' => array(
				'label'    => esc_html__( 'Preset 01', 'vnh' ),
				'settings' => array(
					'header_toggle'        => true,
					'header_sticky_toggle' => true,
				),
			),
			'2' => array(
				'label'    => esc_html__( 'Preset 02', 'vnh' ),
				'settings' => array(
					'header_toggle'        => false,
					'header_sticky_toggle' => false,
				),
			),
		);

		$preset['footer'] = array(
			'footer' => array(
				'1' => array(
					'label'    => esc_html__( 'Preset 01', 'vnh' ),
					'settings' => array(
						'footer_toggle' => true,
					),
				),
				'2' => array(
					'label'    => esc_html__( 'Preset 02', 'vnh' ),
					'settings' => array(
						'footer_toggle' => false,
					),
				),
			),
		);

		return $preset;
	}
}

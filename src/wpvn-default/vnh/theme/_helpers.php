<?php

namespace VNH\Theme;

use VNH\Framework\Kirki;

function is_tablet() {
	if ( class_exists( 'Mobile_Detect' ) ) {
		return ( new \Mobile_Detect() )->isTablet();
	}
}

function is_mobile() {
	if ( class_exists( 'Mobile_Detect' ) && ! is_tablet() ) {
		return ( new \Mobile_Detect() )->isMobile();
	}
}

function is_handheld() {
	if ( class_exists( 'Mobile_Detect' ) ) {
		return ( is_mobile() || is_tablet() );
	}
}

function is_desktop() {
	return ! is_handheld();
}

function require_files( $pattern ) {
	$paths = glob( get_template_directory() . DS . $pattern, GLOB_NOSORT );
	if ( ! $paths ) {
		wp_die( esc_html__( 'Could not load pattern: ', 'vnh' ) . esc_html( $pattern ) );
	}
	foreach ( $paths as $path ) {
		require_once( $path );
	}
}

function get_kirki_option( $setting ) {
	return Kirki::get_option( 'theme', $setting );
}

function get_big_title( $title, $desc = '' ) {
	if ( empty( $desc ) ) {
		return sprintf( '<div class="big_title">%s</div>', $title );
	} else {
		return sprintf( '<div class="big_title">%s</div><div class="desc">%s</div>', $title, $desc );
	}
}

function get_group_title( $title, $icon = '' ) {
	if ( empty( $icon ) ) {
		return sprintf( '<div class="group_title">%s</div>', $title );
	} else {
		return sprintf( '<div class="group_title"><i class="dashicons %s"></i>%s</div>', $icon, $title );
	}
}

function get_layout( $layouts ) {
	$result = array();
	foreach ( $layouts as $layout ) {
		$result[ $layout ] = get_parent_theme_file_uri( THEME_ASSETS_DIR . "images/$layout.png" );
	}

	return $result;
}

function minify_css( $css = '' ) {
	// Return if no CSS
	if ( ! $css ) {
		return;
	}

	// Normalize whitespace
	$css = preg_replace( '/\s+/', ' ', $css );

	// Remove ; before }
	$css = preg_replace( '/;(?=\s*})/', '', $css );

	// Remove space after , : ; { } */ >
	$css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );

	// Remove space before , ; { }
	$css = preg_replace( '/ (,|;|\{|})/', '$1', $css );

	// Strips leading 0 on decimal values (converts 0.5px into .5px)
	$css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );

	// Strips units if value is 0 (converts 0px to 0)
	$css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );

	// Trim
	$css = trim( $css );

	// Return minified CSS
	return $css;

}

function get_first_image() {
	$image   = '';
	$link    = get_permalink();
	$content = get_the_content();
	$count   = substr_count( $content, '<img' );
	$start   = 0;
	for ( $i = 1; $i <= $count; $i++ ) {
		$img_beg     = strpos( $content, '<img', $start );
		$post        = substr( $content, $img_beg );
		$img_end     = strpos( $post, '>' );
		$post_output = substr( $post, 0, $img_end + 1 );
		$post_output = preg_replace( '/width="([0-9]*)" height="([0-9]*)"/', '', $post_output );
		$image[ $i ] = $post_output;
		$start       = $img_end + 1;
	}
	if ( stristr( $image[1], '<img' ) ) {
		return "<a href=$link>$image[1]</a>";
	}
}

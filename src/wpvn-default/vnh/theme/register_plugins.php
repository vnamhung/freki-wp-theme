<?php

namespace VNH\Theme;

class Register_Plugins {
	public $config = array(
		'id'           => 'vnh',                          // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                             // Default absolute path to bundled plugins.
		'menu'         => 'vnh-install-plugins',          // Menu slug.
		'has_notices'  => true,                           // Show admin notices or not.
		'dismissable'  => true,                           // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                             // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                           // Automatically activate plugins after installation or not.
		'message'      => '',                             // Message to output right before the plugins table.
	);

	public $plugins = array();

	public function __construct() {
		add_action( 'tgmpa_register', [ $this, 'register_plugins' ] );
	}

	public function register_plugins() {
		/*
		 * Array of plugin arrays. Required keys are name and slug.
		 * If the source is NOT from the .org repo, then source is also required.
		 */
		$this->plugins = array(
			array(
				'name'     => esc_html__( 'Contact Form 7', 'vnh' ),
				'slug'     => 'contact-form-7',
				'required' => false,
			),
			array(
				'name'     => esc_html__( 'Advanced Forms', 'vnh' ),
				'slug'     => 'advanced-forms',
				'required' => false,
			),
		);

		return tgmpa( $this->plugins, $this->config );
	}
}

<?php

namespace VNH\Theme;

use VNH\Framework\Kirki;

$section  = 'site';
$priority = 1;

/*--------------------------------------------------------------
# Sample separator comment
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_big_title( __( 'Layout', 'vnh' ), __( 'Controls the color of all menu item links.', 'vnh' ) ),
	// parameter 2 can blank
) );

Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_group_title( __( 'Custom CSS', 'vnh' ), 'dashicons-admin-appearance' ), // parameter 2 can blank
) );

Kirki::add_field( 'theme', array(
	'type'        => 'radio-image',
	'settings'    => 'page_layout',
	'label'       => __( 'Page', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 'fullwidth',
	'choices'     => get_layout( [ 'fullwidth', 'content-sidebar' ] ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'preset',
	'settings'    => 'header_preset',
	'description' => __( 'Choose a preset you want', 'vnh' ),
	'label'       => __( 'Preset', 'vnh' ),
	'section'     => $section,
	'default'     => '1',
	'priority'    => $priority++,
	'multiple'    => 3,
	'choices'     => array(
		'1' => array(
			'label'    => Customize::preset()['header']['1']['label'],
			'settings' => Customize::preset()['header']['1']['settings'],
		),
		'2' => array(
			'label'    => Customize::preset()['header']['2']['label'],
			'settings' => Customize::preset()['header']['2']['settings'],
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'radio-image',
	'settings'    => 'blog_layout',
	'label'       => __( 'Blog', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 'content-sidebar',
	'transport'   => 'postMessage',
	'choices'     => get_layout( [ 'fullwidth', 'content-sidebar' ] ),
	'js_vars'     => array(
		array(
			'element'  => 'body',
			'function' => 'html',
			'attr'     => 'data-blog',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'link_color',
	'label'       => __( 'Normal', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'choices'     => array(
		'alpha' => true,
	),
	'transport'   => 'auto',
	'default'     => PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => 'a,a:visited',
			'property' => 'color',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'typography',
	'settings'    => 'site_body_typo',
	'label'       => __( 'Font family', 'vnh' ),
	'description' => __( 'These settings control the typography for all body text.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => PRIMARY_FONT,
		'variant'        => 'regular',
		'font-size'      => '16px',
		'color'          => SECONDARY_COLOR,
		'line-height'    => '1.5',
		'letter-spacing' => '0em',
		'subsets'        => array( 'latin-ext' ),
	),
	'choices'     => array(
		'variant' => array( 'regular', 'italic', '700', '700italic' ),
	),
	'output'      => array(
		array(
			'element' => 'body',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h1_font_size',
	'label'       => __( 'Font size', 'vnh' ),
	'description' => __( 'H1', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 28,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => 'h1,.h1',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

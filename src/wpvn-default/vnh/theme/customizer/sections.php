<?php

use VNH\Framework\Kirki;

$priority = 1;
Kirki::add_section( 'site', array(
	'title'       => __( 'Site', 'vnh' ),
	'description' => __( 'Easily adjust your site\'s copyright by setting your widget areas to the specific number desired and turning on or off various parts as needed. You\'re never forced to use a layout you don\'t need with theme.', 'vnh' ),
	'priority'    => $priority++,
) );

Kirki::add_section( 'header', array(
	'title'       => __( 'Header', 'vnh' ),
	'description' => __( 'Never before has such flexibility been offered to WordPress users for their site\'s header. It\'s one of the first things your visitors see when they come to your site, now you can make it look exactly how you want. ', 'vnh' ),
	'priority'    => $priority++,
) );

Kirki::add_section( 'socials', array(
	'title'       => __( 'Socials', 'vnh' ),
	'description' => __( ' Never before has such flexibility been offered to WordPress users for their site\'s header. It\'s one of the first things your visitors see when they come to your site, now you can make it look exactly how you want.', 'vnh' ),
	'priority'    => $priority++,
) );

// Menu panel setup.
Kirki::add_section( 'navigation', array(
	'title'       => __( 'Desktop Menu Settings', 'vnh' ),
	'description' => __( 'Easily adjust your site\'s copyright by setting your widget areas to the specific number desired and turning on or off various parts as needed. You\'re never forced to use a layout you don\'t need with theme.', 'vnh' ),
	'priority'    => 1,
	'panel'       => 'nav_menus',
) );

Kirki::add_section( 'navigation_mobile', array(
	'title'       => __( 'Mobile Menu Settings', 'vnh' ),
	'description' => __( 'Easily adjust your site\'s copyright by setting your widget areas to the specific number desired and turning on or off various parts as needed. You\'re never forced to use a layout you don\'t need with theme.', 'vnh' ),
	'priority'    => 2,
	'panel'       => 'nav_menus',
) );

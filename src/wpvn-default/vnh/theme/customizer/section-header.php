<?php

namespace VNH\Theme;

use VNH\Framework\Kirki;

$section  = 'header';
$priority = 1;

/*--------------------------------------------------------------
# General
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_group_title( __( 'General', 'vnh' ), 'dashicons-admin-settings' ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'preset',
	'settings'    => 'header_preset',
	'description' => __( 'Choose a preset you want', 'vnh' ),
	'label'       => __( 'Preset', 'vnh' ),
	'section'     => $section,
	'default'     => '1',
	'priority'    => $priority++,
	'multiple'    => 3,
	'choices'     => array(
		'1' => array(
			'label'    => Customize::preset()['header']['1']['label'],
			'settings' => Customize::preset()['header']['1']['settings'],
		),
		'2' => array(
			'label'    => Customize::preset()['header']['2']['label'],
			'settings' => Customize::preset()['header']['2']['settings'],
		),
	),
) );

/*--------------------------------------------------------------
# Header layout
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_group_title( __( 'Layout', 'vnh' ), 'dashicons-layout' ),
) );

Kirki::add_field( 'theme', array(
	'type'            => 'toggle',
	'settings'        => 'header_toggle',
	'label'           => __( 'Visibility', 'vnh' ),
	'description'     => __( 'Non risus fringilla neque at convallis in.', 'vnh' ),
	'section'         => $section,
	'priority'        => $priority++,
	'default'         => true,
	'partial_refresh' => array(
		'header_visibility' => array(
			'selector'            => '.header',
			'container_inclusive' => true,
			'render_callback'     => function() {
				get_template_part( 'header' );
			},
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => 'header_sticky_toggle',
	'label'       => __( 'Sticky', 'vnh' ),
	'description' => __( 'Non risus fringilla neque at convallis in.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => false,
) );

Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'header_fixed_radio',
	'label'       => __( 'Fixed position', 'vnh' ),
	'description' => __( 'Non risus fringilla neque at convallis in.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 'disable',
	'transport'   => 'postMessage',
	'choices'     => array(
		'enable'  => __( 'Enable', 'vnh' ),
		'disable' => __( 'Disable', 'vnh' ),
	),
	'js_vars'     => array(
		array(
			'element'  => '.header',
			'function' => 'html',
			'attr'     => 'fixed',
		),
	),
) );

/*--------------------------------------------------------------
# Header color
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_group_title( __( 'Color', 'vnh' ), 'dashicons-art' ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => 'header_bg_color',
	'label'       => __( 'Background', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'transport'   => 'auto',
	'default'     => 'rgba(255,255,255,.3)',
	'output'      => array(
		array(
			'element'  => '.header',
			'property' => 'background-color',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => 'header_border_color',
	'label'       => __( 'Border', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'transport'   => 'auto',
	'default'     => PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '.header',
			'property' => 'border-color',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => 'header_shadow_color',
	'label'       => __( 'Shadow', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'transport'   => 'auto',
	'default'     => 'rgba(0, 0, 0, 0.15)',
	'output'      => array(
		array(
			'element'       => '.header-sample',
			'property'      => '-moz-box-shadow',
			'value_pattern' => '0 1px 3px $',
		),
		array(
			'element'       => '.header-sample',
			'property'      => '-webkit-box-shadow',
			'value_pattern' => '0 1px 3px $',
		),
	),
) );

/*--------------------------------------------------------------
# Search title
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_big_title( __( 'Search', 'vnh' ), __( 'Donec tristique pharetra dolor vitae.', 'vnh' ) ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => 'header_search_toggle',
	'label'       => __( 'Visibility', 'vnh' ),
	'description' => __( 'Non risus fringilla neque at convallis in.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => true,
) );

/*--------------------------------------------------------------
# Minicart title
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_big_title( __( 'Mini cart', 'vnh' ) ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => 'header_minicart_toggle',
	'label'       => __( 'Visibility', 'vnh' ),
	'description' => __( 'Non risus fringilla neque at convallis in.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => false,
) );

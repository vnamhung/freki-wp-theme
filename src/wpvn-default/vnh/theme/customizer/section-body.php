<?php

namespace VNH\Theme;

use VNH\Framework\Kirki;

$section  = 'site';
$priority = 1;

/*--------------------------------------------------------------
# Layout
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_big_title( __( 'Layout', 'vnh' ), __( 'Maecenas eleifend gravida ex vel.', 'vnh' ) ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'radio-image',
	'settings'    => 'site_layout',
	'label'       => __( 'Site', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 'content-sidebar',
	'transport'   => 'postMessage',
	'choices'     => get_layout( [ 'sidebar-content', 'content-sidebar' ] ),
	'js_vars'     => array(
		array(
			'element'  => '.site',
			'function' => 'html',
			'attr'     => 'data-layout',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'radio-image',
	'settings'    => 'page_layout',
	'label'       => __( 'Page', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 'fullwidth',
	'transport'   => 'postMessage',
	'choices'     => get_layout( [ 'fullwidth', 'content-sidebar' ] ),
	'js_vars'     => array(
		array(
			'element'  => '.site',
			'function' => 'html',
			'attr'     => 'data-layout',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'radio-image',
	'settings'    => 'post_layout',
	'label'       => __( 'Post', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 'sidebar-content',
	'transport'   => 'postMessage',
	'choices'     => get_layout( [ 'content-sidebar', 'sidebar-content' ] ),
	'js_vars'     => array(
		array(
			'element'  => '.site',
			'function' => 'html',
			'attr'     => 'data-layout',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'radio-image',
	'settings'    => 'archive_layout',
	'label'       => __( 'Archive', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 'content-sidebar',
	'transport'   => 'postMessage',
	'choices'     => get_layout( [ 'fullwidth', 'content-sidebar', 'sidebar-content' ] ),
	'js_vars'     => array(
		array(
			'element'  => '.site',
			'function' => 'html',
			'attr'     => 'data-layout',
		),
	),
) );

/*--------------------------------------------------------------
# Link color
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_big_title( __( 'Link color', 'vnh' ), __( 'Donec diam neque, egestas non.', 'vnh' ) ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'link_color',
	'label'       => __( 'Normal', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'choices'     => array(
		'alpha' => true,
	),
	'transport'   => 'auto',
	'default'     => PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => 'a,a:visited',
			'property' => 'color',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'link_color_hover',
	'label'       => __( 'Hover', 'vnh' ),
	'description' => __( 'Controls the color of all menu item links.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'transport'   => 'auto',
	'default'     => SECONDARY_COLOR,
	'output'      => array(
		array(
			'element'  => 'a:hover',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Body typography
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_big_title( __( 'Body typography', 'vnh' ), __( 'Ut erat neque, rutrum ac.', 'vnh' ) ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'typography',
	'settings'    => 'site_body_typo',
	'label'       => __( 'Font family', 'vnh' ),
	'description' => __( 'These settings control the typography for all body text.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => SECONDARY_FONT,
		'variant'        => 'regular',
		'font-size'      => '16px',
		'color'          => PRIMARY_COLOR,
		'line-height'    => '1.5',
		'letter-spacing' => '0em',
		'subsets'        => array( 'latin-ext' ),
	),
	'choices'     => array(
		'variant' => array( 'regular', 'italic', '700', '700italic' ),
	),
	'output'      => array(
		array(
			'element' => 'body',
		),
	),
) );

/*--------------------------------------------------------------
# Heading typography
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_big_title( __( 'Heading typography', 'vnh' ), __( 'Duis aliquet scelerisque sem, quis.', 'vnh' ) ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'typography',
	'settings'    => 'heading_typo',
	'label'       => __( 'Font family', 'vnh' ),
	'description' => __( 'These settings control the typography for all heading text.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => PRIMARY_FONT,
		'variant'        => '700',
		'color'          => PRIMARY_COLOR,
		'line-height'    => '1.5',
		'letter-spacing' => '0em',
		'subsets'        => array( 'latin-ext' ),
	),
	'output'      => array(
		array(
			'element' => 'h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h1_font_size',
	'label'       => __( 'Font size', 'vnh' ),
	'description' => __( 'H1', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 28,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => 'h1,.h1',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h2_font_size',
	'description' => __( 'H2', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 24,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => 'h2,.h2',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h3_font_size',
	'description' => __( 'H3', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 20,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => 'h3,.h3',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h4_font_size',
	'description' => __( 'H4', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 18,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => 'h4,.h4',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h5_font_size',
	'description' => __( 'H5', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 16,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => 'h5,.h5',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h6_font_size',
	'description' => __( 'H6', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'default'     => 14,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => 'h6,.h6',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

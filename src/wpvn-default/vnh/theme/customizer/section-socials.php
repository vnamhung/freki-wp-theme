<?php

namespace VNH\Theme;

use VNH\Framework\Kirki;

$section  = 'socials';
$priority = 1;

/*--------------------------------------------------------------
# Social links
--------------------------------------------------------------*/
Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => uniqid(),
	'section'  => $section,
	'priority' => $priority++,
	'default'  => get_group_title( __( 'Social links', 'vnh' ) ),
) );

Kirki::add_field( 'theme', array(
	'type'        => 'repeater',
	'settings'    => 'social_link',
	'description' => __( 'You can find icon class <a target="_blank" href="http://fontawesome.io/cheatsheet/">here</a>.', 'vnh' ),
	'section'     => $section,
	'priority'    => $priority++,
	'row_label'   => array(
		'type'  => 'field',
		'field' => 'tooltip',
	),
	'default'     => array(
		array(
			'tooltip'    => 'Facebook',
			'icon_class' => 'fa-facebook',
			'link_url'   => 'https://facebook.com',
		),
		array(
			'tooltip'    => 'Twitter',
			'icon_class' => 'fa-twitter',
			'link_url'   => 'https://twitter.com',
		),
		array(
			'tooltip'    => 'Pinterest',
			'icon_class' => 'fa-pinterest',
			'link_url'   => 'https://pinterest.com',
		),
		array(
			'tooltip'    => 'Instagram',
			'icon_class' => 'fa-instagram',
			'link_url'   => 'https://www.instagram.com',
		),
	),
	'fields'      => array(
		'tooltip'    => array(
			'type'        => 'text',
			'label'       => __( 'Tooltip', 'vnh' ),
			'description' => __( 'Enter your hint text for your icon', 'vnh' ),
			'default'     => '',
		),
		'icon_class' => array(
			'type'        => 'text',
			'label'       => __( 'Font Awesome Class', 'vnh' ),
			'description' => __( 'This will be the icon class for your link', 'vnh' ),
			'default'     => '',
		),
		'link_url'   => array(
			'type'        => 'text',
			'label'       => __( 'Link URL', 'vnh' ),
			'description' => __( 'This will be the link URL', 'vnh' ),
			'default'     => '',
		),
	),
) );

<?php

namespace VNH\Theme;

function posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string, esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ), esc_attr( get_the_modified_date( 'c' ) ), esc_html( get_the_modified_date() ) );

	/* translators: %s: Name of current post. */
	$posted_on = sprintf( esc_html_x( 'Posted on %s', 'post date', 'vnh' ), '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>' );

	/* translators: %s: Name author. */
	$byline = sprintf( esc_html_x( 'by %s', 'post author', 'vnh' ), '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>' );

	echo '<div class="entry-meta"><span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span></div><!-- .entry-meta -->'; // WPCS: XSS OK.
}

function entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'vnh' ) );
		if ( $categories_list && categorized_blog() ) {
			/* translators: %s: Name of category list. */
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'vnh' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'vnh' ) );
		if ( $tags_list ) {
			/* translators: %s: Name of tag list. */
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'vnh' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'vnh' ), esc_html__( '1 Comment', 'vnh' ), esc_html__( '% Comments', 'vnh' ) );
		echo '</span>';
	}
	edit_link();
}

function edit_link() {
	/* translators: %s: Name of current post */
	edit_post_link( sprintf( __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'vnh' ), get_the_title() ), '<span class="edit-link">', '</span>' );
}

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function categorized_blog() {
	$all_the_cool_cats = get_transient( 'vnh_categories' );
	if ( $all_the_cool_cats === false ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'vnh_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so categorized_blog should return false.
		return false;
	}
}

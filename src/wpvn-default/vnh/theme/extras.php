<?php

namespace VNH\Theme;

use VNH\Framework\Extras_Base;

class Extras extends Extras_Base {
	public $pingback_header      = true;

	public $theme_meta_generator = true;

	public $add_async            = true;

	public $template_redirect    = true;

	public function __construct() {
		parent::__construct();

		add_filter( 'body_class', [ $this, 'body_classes' ] );

//		add_filter( 'widget_tag_cloud_args', [ $this, 'widget_tag_cloud_args' ] );

//		add_filter( 'wp_list_categories', [ $this, 'categories_post_count_filter' ] );

//		add_filter( 'get_archives_link', [ $this, 'archives_post_count_filter' ] );
	}

	public function body_classes( $classes ) {
		// Adds a class of group-blog to blog with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}

		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		// Adds a class for mobile device.
		if ( is_mobile() ) {
			$classes[] = 'mobile';
		}

		// Adds a class for tablet device.
		if ( is_tablet() ) {
			$classes[] = 'tablet';
		}

		// Adds a class for handheld device.
		if ( is_handheld() ) {
			$classes[] = 'handheld';
		}

		// Adds a class for desktop device.
		if ( is_desktop() ) {
			$classes[] = 'desktop';
		}

		return $classes;
	}

	public function widget_tag_cloud_args( $args ) {
		$args['largest']  = 14;
		$args['smallest'] = 14;
		$args['unit']     = 'px';

		return $args;
	}

	public function categories_post_count_filter( $variable ) {
		$variable = str_replace( '(', '<span class="post_count"> ', $variable );
		$variable = str_replace( ')', '</span>', $variable );

		return $variable;
	}

	public function archives_post_count_filter( $variable ) {
		$variable = str_replace( '(', '<span class="post_count"> ', $variable );
		$variable = str_replace( ')', '</span>', $variable );

		return $variable;
	}
}

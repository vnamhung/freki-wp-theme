<?php

namespace VNH\Theme;

use VNH\Framework\Compatibility_Base;

class Compatibility extends Compatibility_Base {
	public $min_version = WP_MIN_VERSION;

	public function __construct() {
		if ( version_compare( $GLOBALS['wp_version'], $this->min_version , '<' ) ) :
			parent::_init();
		endif;
	}
}

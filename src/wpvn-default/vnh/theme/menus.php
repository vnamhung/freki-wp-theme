<?php

namespace VNH\Theme;

class Menus {
	public $declaration = array();

	public function __construct() {
		$this->declaration = array(
			'primary' => esc_html__( 'Primary', 'vnh' ),
		);

		add_action( 'after_setup_theme', [ $this, 'register_nav_menus' ] );

		add_filter( 'wp_page_menu_args', [ $this, 'page_menu_args' ] );
	}

	public function register_nav_menus() {
		register_nav_menus( $this->declaration );
	}

	public function page_menu_args( $args ) {
		$args['show_home'] = true;

		return $args;
	}
}

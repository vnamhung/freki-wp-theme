<?php

namespace VNH\Theme;

class Widgets {
	public $declaration = array();

	public function __construct() {
		$this->declaration = array(
			'sidebar' => array(
				'name'        => esc_html__( 'Sidebar', 'vnh' ),
				'description' => esc_html__( 'Add widgets here.', 'vnh' ),
			),
			'footer'  => array(
				'name'        => esc_html__( 'Footer', 'vnh' ),
				'description' => esc_html__( 'Add widgets here.', 'vnh' ),
			),
		);

		add_action( 'widgets_init', [ $this, 'widgets_init' ] );
	}

	public function widgets_init() {
		foreach ( $this->declaration as $id => $prop ) {
			$this->reg_widget( $id, $prop['name'], $prop['description'] );
		}

		// Allow the Text Widgets to handle shortcodes
		add_filter( 'widget_text', 'shortcode_unautop' );
		add_filter( 'widget_text', 'do_shortcode' );
	}

	protected function reg_widget( $id, $name, $desc ) {
		register_sidebar( array(
			'id'            => $id,
			'name'          => $name,
			'description'   => $desc,
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget__title">',
			'after_title'   => '</h3>',
		) );
	}
}

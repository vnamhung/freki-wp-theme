<?php

namespace VNH\Theme;

use VNH\Framework\Framework_Support_Base;

class Framework_Support extends Framework_Support_Base {
	public $support = array();

	public function __construct() {
		$this->support['core'] = array( // fixit: enable these options base on theme request.
			'mobile_detect'         => true,
			'merlin'                => true,
			'kirki'                 => true,
			'acf'                   => true,
			'bfi_thumb'             => true,
			'tgm_plugin_activation' => true,
			'kint'                  => true,
		);

		$this->support['extra'] = array( // fixit: enable these options base on theme request.
			'barba'  => true,
			'retina' => true,
		);

		$this->support['dev'] = array( // fixit: switch to false or delete these options before theme release.
			'show_acf_admin' => true,
			'acf_local_json' => true,
			'browser_sync'   => true,
		);

		add_action( 'after_setup_theme', [ $this, 'declare_framework_supports' ], -1 );

		parent::__construct();
	}

	public function declare_framework_supports() {
		add_theme_support( 'framework', $this->support );
	}
}

<?php

namespace VNH\Theme;

use VNH\Framework\Security_Base;

class Security extends Security_Base {
	public $head_clean_up = true;

	public $remove_version = false;

	public $prevent_bad_query = true;

	public function __construct() {
		parent::__construct();

//		add_filter( 'show_admin_bar', '__return_false' );
	}
}

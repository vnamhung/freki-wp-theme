<?php

namespace VNH\Theme;

function layout() {
	if ( is_page() ) {
		$layout = get_kirki_option( 'page_layout' );
	} elseif ( is_archive() ) {
		$layout = get_kirki_option( 'archive_layout' );
	} elseif ( is_single() ) {
		$layout = get_kirki_option( 'post_layout' );
	} else {
		$layout = get_kirki_option( 'site_layout' );
	}

	switch ( $layout ) {
		case 'fullwidth':
			echo 'fullwidth';
			break;
		case 'content-sidebar':
			echo 'content-sidebar';
			break;
		case 'sidebar-content':
			echo 'sidebar-content';
			break;
	}
}

function fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'vnh' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => rawurlencode( implode( '|', $fonts ) ),
			'subset' => rawurlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}

function cached_query( $args ) {
	$query_id = md5( maybe_serialize( $args ) );
	$query    = wp_cache_get( $query_id, 'library' );
	if ( false === $query ) {
		$query = new \WP_Query( $args );
		wp_cache_set( $query_id, $query, 'library' );
	}

	return $query;
}

function cached_get_posts( $args ) {
	$query = cached_query( $args );

	return $query->posts;
}

function get_image_id( $image_url ) {
	if ( empty( $image_url ) ) {
		return false;
	}

	if ( is_shop() ) {
		$attachment_id = wp_cache_get( $image_url, 'vnh_image_id' );
	}

	if ( $attachment_id === false ) {
		global $wpdb;
		$attachment    = $wpdb->get_col(
			$wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )
		);
		$attachment_id = ! empty( $attachment[0] ) ? $attachment[0] : 0;
		wp_cache_set( $image_url, $attachment_id, 'vnh_image_id', 86400 );
	}

	return $attachment_id;
}

function get_related_posts( $post_id, $number_posts = -1 ) {

	$args = '';

	if ( 0 === $number_posts ) {
		$query = new \WP_Query();

		return $query;
	}

	$args = wp_parse_args( $args, array(
		'category__in'        => wp_get_post_categories( $post_id ),
		'ignore_sticky_posts' => 0,
		'posts_per_page'      => $number_posts,
		'post__not_in'        => array( $post_id ),
	) );

	return cached_query( $args );

}

function get_custom_posttype_related_posts( $post_id, $number_posts = 8, $post_type = 'vnh_portfolio' ) {

	$query = new \WP_Query();

	$args = '';

	if ( 0 === $number_posts ) {
		return $query;
	}

	$post_type = str_replace( 'vnh_', '', $post_type );

	$item_cats = get_the_terms( $post_id, $post_type . '_category' );

	$item_array = array();
	if ( $item_cats ) {
		foreach ( $item_cats as $item_cat ) {
			$item_array[] = $item_cat->term_id;
		}
	}

	if ( ! empty( $item_array ) ) {
		$args = wp_parse_args( $args, array(
			'ignore_sticky_posts' => 0,
			'posts_per_page'      => $number_posts,
			'post__not_in'        => array( $post_id ),
			'post_type'           => 'vnh_' . $post_type,
			'tax_query'           => array(
				array(
					'field'    => 'id',
					'taxonomy' => $post_type . '_category',
					'terms'    => $item_array,
				),
			),
		) );

		$query = cached_query( $args );

	}

	return $query;
}

function get_featured_image_class() {
	$url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
	list( $width, $height ) = getimagesize( $url );

	$content = get_the_content();

	if ( $width > $height || $width === $height ) {
		$thumb_class = 'featured-landscape';
	} elseif ( empty( $content ) ) {
		$thumb_class = 'featured-no-content';
	} else {
		$thumb_class = 'featured-portrait';
	}

	return $thumb_class;
}

function the_menu_primary() {
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container'      => false,
		'menu_class'     => 'menu__container',
		'fallback_cb'    => function() {
			esc_html_e( 'Please choose a menu', 'vnh' );
		},
	) );
}

function the_custom_excerpt( $num ) {
	$link    = get_permalink();
	$ending  = get_option( 'wl_excerpt_ending' );
	$limit   = $num + 1;
	$excerpt = explode( ' ', get_the_excerpt(), $limit );
	array_pop( $excerpt );
	$excerpt = implode( ' ', $excerpt ) . $ending;
	echo '<p>' . wp_kses_post( $excerpt ) . '</p>';
	$read_more = get_option( 'wl_readmore_link' );
	if ( $read_more !== '' ) {
		$read_more = '<p class="readmore"><a href="' . $link . '">' . $read_more . '</a></p>';
		echo esc_url( $read_more );
	}
}

function get_all_menus() {
	$args      = array(
		'hide_empty' => true,
		'fields'     => 'id=>name',
		'slug'       => '',
	);
	$menus     = get_terms( 'nav_menu', $args );
	$menus[''] = esc_html__( 'Default Menu', 'vnh' );

	return $menus;
}

function get_registered_sidebars( $default_option = false ) {
	global $wp_registered_sidebars;
	$sidebars = array();
	if ( $default_option === true ) {
		$sidebars['default'] = esc_html__( 'Default', 'vnh' );
	}
	foreach ( $wp_registered_sidebars as $sidebar ) {
		$sidebars[ $sidebar['id'] ] = $sidebar['name'];
	}

	return $sidebars;
}

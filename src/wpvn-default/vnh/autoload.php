<?php

namespace VNH;

final class Autoload {
	private static $instance;

	const THEME_DIR = 'vnh/theme';

	const FRAMEWORK_PATH = 'vnh/framework/_framework.php';

	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct() {
		$this->_includes();

		spl_autoload_register( [ $this, 'auto_load' ] );
	}

	protected function auto_load( $class_name ) {
		if ( stripos( $class_name, __NAMESPACE__ ) !== 0 ) {
			return; // Not a VNH file, early exit.
		}

		$class_path = get_parent_theme_file_path( strtolower( str_replace( '\\', '/', $class_name ) ) . '.php' );

		if ( file_exists( $class_path ) ) {
			require_once $class_path;
		}
	}

	private function _includes() {
		require_once get_parent_theme_file_path( self::FRAMEWORK_PATH );
		require_once get_parent_theme_file_path( trailingslashit( self::THEME_DIR ) . '_helpers.php' );
		require_once get_parent_theme_file_path( trailingslashit( self::THEME_DIR ) . '_templates-tags.php' );
		require_once get_parent_theme_file_path( trailingslashit( self::THEME_DIR ) . '_theme.php' );
	}
}

// Require framework files
Autoload::instance();

// Init framework
Run::instance();


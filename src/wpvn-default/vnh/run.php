<?php

namespace VNH;

use VNH\Framework\I18n;
use VNH\Framework\Wrapper;
use VNH\Framework\Kirki;
use VNH\Theme\Compatibility;
use VNH\Theme\Enqueue;
use VNH\Theme\Framework_Support;
use VNH\Theme\Register_Plugins;
use VNH\Theme\Security;
use VNH\Theme\Setup_Merlin;
use VNH\Theme\ACF;
use VNH\Theme\Customize;
use VNH\Theme\Extras;
use VNH\Theme\Menus;
use VNH\Theme\Setup_Theme;
use VNH\Theme\Widgets;
use VNH\Woocommerce\Minicart;
use VNH\Woocommerce\Begin;
use VNH\Woocommerce\Utilities;

final class Run {
	/**
	 * @var Run
	 */
	public static $now;

	/**
	 * @var Compatibility
	 */
	public $compat;
	/**
	 * @var Extras
	 */
	public $extras;

	/**
	 * @var Enqueue
	 */
	public $enqueue;

	/**
	 * @var ACF
	 */
	public $acf;

	/**
	 * @var Menus
	 */
	public $menus;

	/**
	 * @var Setup_Merlin
	 */
	public $merlin;

	/**
	 * @var Customize
	 */
	public $customize;

	/**
	 * @var Security
	 */
	public $security;

	/**
	 * @var Setup_Theme
	 */
	public $setup;

	/**
	 * @var Widgets
	 */
	public $widgets;

	/**
	 * @var Framework_Support
	 */
	public $framework;

	/**
	 * @var Register_Plugins
	 */
	public $plugins;

	/**
	 * @return Run
	 */
	public static function instance() {
		if ( is_null( self::$now ) ) {
			self::$now = new self();
		}

		return self::$now;
	}

	private function __construct() {
		$this->declare_components();
		do_action( 'vnh/h/init' );
	}

	protected function declare_components() {
		/*
		 * VNH\Framework
		 */
		new I18n();
		new Wrapper(); // A Smarter Way to Load Header and Footer Elements
		new Kirki(); // This is a wrapper class for Kirki

		/*
		 * VNH\Theme
		 */
		$this->compat    = new Compatibility();
		$this->framework = new Framework_Support();
		$this->enqueue   = new Enqueue();
		$this->acf       = new ACF();
		$this->customize = new Customize();
		$this->menus     = new Menus();
		$this->setup     = new Setup_Theme();
		$this->widgets   = new Widgets();
		$this->plugins   = new Register_Plugins();
		$this->extras    = new Extras();
		$this->security  = new Security();
		$this->merlin    = new Setup_Merlin();

		/*
		 * VNH\Woocommerce
		 */
		new Begin();
		new Utilities();
		new Minicart();
	}
}

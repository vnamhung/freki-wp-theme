<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 */

if ( have_posts() ) :

	if ( is_home() && ! is_front_page() ) :

		printf( '<header><h1 class="page-title screen-reader-text">%s</h1></header>', single_post_title() );

	endif;

	while ( have_posts() ) :
		the_post();
		get_template_part( 'template-parts/post/content', get_post_format() );

	endwhile;

	the_posts_pagination( array(
		'prev_text'          => __( 'Previous page', 'vnh' ),
		'next_text'          => __( 'Next page', 'vnh' ),
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'vnh' ) . ' </span>',
	) );

else :

	get_template_part( 'template-parts/post/content', 'none' );

endif;


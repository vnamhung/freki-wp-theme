<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title">
			<?php esc_html_e( 'Nothing Found', 'vnh' ); ?>
		</h1>
	</header>
	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) {
			/* translators: %s: Link to write new post. */
			printf( '<p>' . wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'vnh' ) . '</p>', array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) );
		} elseif ( is_search() ) {
			echo '<p>';
			esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'vnh' );
			echo '</p>';
			get_search_form();
		} else {
			echo '<p>';
			esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'vnh' );
			echo '</p>';
			get_search_form();

		};
		?>
	</div>
</section><!-- .no-results -->

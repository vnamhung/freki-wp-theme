<div class="header__left site__branding">
	<?php if ( is_front_page() ) : ?>
		<h1 class="site__title">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <?php bloginfo( 'name' ); ?> </a>
		</h1><!-- .site__title -->
	<?php else : ?>
		<p class="site__title">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <?php bloginfo( 'name' ); ?> </a>
		</p><!-- .site__title -->
	<?php endif; ?>
	<p class="site__description"> <?php echo esc_html( get_bloginfo( 'description', 'display' ) ); ?> </p><!-- .site__description -->
</div><!-- .site__branding -->

<?php if ( has_nav_menu( 'primary' ) ) : ?>
<nav id="site__navigation" class="header__right site__navigation" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">
	<?php
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'menu_id'        => 'header__navigation',
	) );
	?>
</nav><!-- #site__navigation-->
<?php endif; ?>

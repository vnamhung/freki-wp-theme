<?php
/**
 * Define constants
 */
define( 'VNH\THEME\DS', DIRECTORY_SEPARATOR );
define( 'VNH\THEME\WP_MIN_VERSION', '4.7' );
define( 'VNH\THEME\PARENT_THEME_VERSION', wp_get_theme( get_template() )->get( 'Version' ) );
define( 'VNH\THEME\LIBS_DIR', trailingslashit( 'assets/libs' ) );
define( 'VNH\THEME\CUSTOMIZER_DIR', trailingslashit( 'vnh/theme/customizer' ) );
define( 'VNH\THEME\THEME_ASSETS_DIR', trailingslashit( 'vnh/theme/assets' ) );
define( 'VNH\THEME\SAMPLE_DIR', trailingslashit( 'vnh/sample' ) );
define( 'VNH\THEME\GOOGLE_API_KEY', 'AIzaSyCwAq_RDWPBbKWITpsF1TH5V3tRgrrNX9w' );

define( 'VNH\THEME\PRIMARY_FONT', 'Montserrat' ); //fixit: change them as you want.
define( 'VNH\THEME\SECONDARY_FONT', 'Merriweather' );
define( 'VNH\THEME\PRIMARY_COLOR', '#000' );
define( 'VNH\THEME\SECONDARY_COLOR', '#007acc' );

/**
 * Load Framework
 */
require_once get_parent_theme_file_path( 'vnh/autoload.php' );

/**
 * Please Note: Do not add any custom code here. Please use the following methods to customize the theme so that your customizations are not lost during updates.
 * 1. Child Theme http://codex.wordpress.org/Child_Themes
 * 2. Install Custom css-js-php plugin  https://wordpress.org/plugins/custom-css-js-php/
 */

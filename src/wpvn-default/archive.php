<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package VNH
 */

if ( have_posts() ) :

	printf( '<header class="page-header"><h1 class="page-title">%1s</h1><div class="taxonomy-description">%2s</div></header>', esc_html( get_the_archive_title() ), esc_html( get_the_archive_description() ) );

	while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/post/content', get_post_format() );

	endwhile;

	the_posts_pagination( array(
		'prev_text'          => __( 'Previous page', 'vnh' ),
		'next_text'          => __( 'Next page', 'vnh' ),
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'vnh' ) . ' </span>',
	) );

else :

	get_template_part( 'template-parts/post/content', 'none' );

endif;

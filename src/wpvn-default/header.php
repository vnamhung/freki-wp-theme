<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 */

use function VNH\Theme\get_kirki_option;

?>
<header id="masthead" class="header" itemscope itemtype="http://schema.org/WPHeader">
	<?php if ( get_kirki_option( 'header_toggle' ) === true ) : ?>
		<div class="header__wrapper">
			<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
			<?php get_template_part( 'template-parts/header/navigation', 'top' ); ?>
		</div><!-- .header__wrapper -->
	<?php endif; ?>
</header><!-- .header -->

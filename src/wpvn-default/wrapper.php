<?php
use VNH\Framework\Wrapper;
use function VNH\Theme\layout;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
		<?php do_action( 'vnh/h/after/body' ); ?>
		<div id="page" class="site" data-transition="fadeIn" data-layout="<?php layout(); ?>">

			<?php get_header( Wrapper::get_base() ); ?>

			<div id="content" class="content">
				<div class="content__wrapper">

					<main id="primary" class="content__left content-area" role="main">
						<?php load_template( Wrapper::get_main_template() ); ?>
					</main><!-- .content-area -->

					<aside id="secondary" class="content__right widget-area" role="complementary" aria-label="Primary Sidebar" itemscope itemtype="http://schema.org/WPSideBar">
						<?php get_sidebar( Wrapper::get_base() ); ?>
					</aside><!-- .widget-area -->

				</div><!-- .content__wrapper -->
			</div><!-- .content -->

			<?php get_footer( Wrapper::get_base() ); ?>

		</div><!-- .site -->
		<?php wp_footer(); ?>
	</body>
</html>

<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 */

while ( have_posts() ) :
	the_post();

	get_template_part( 'template-parts/post/content', get_post_format() );

	the_post_navigation();

	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}

endwhile; // End of the loop.

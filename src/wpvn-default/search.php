<?php
/**
 * The template for displaying search results pages.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 */

if ( have_posts() ) :
	/* translators: %s: Search keyword. */
	printf( '<header class="page-header"><h1 class="page-title">' . esc_html__( 'Search Results for: %s', 'vnh' ) . '</h1></header>', get_search_query() );

	while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/post/content', 'search' );

	endwhile;

	the_posts_pagination( array(
		'prev_text'          => __( 'Previous page', 'vnh' ),
		'next_text'          => __( 'Next page', 'vnh' ),
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'vnh' ) . ' </span>',
	) );

else :

	get_template_part( 'template-parts/post/content', 'none' );

endif;

(
	function( vnh, $ ) {
		vnh = vnh || {};
		$.extend( vnh, {

			FancyBox: {

				init: function() {
					this.build();
					return this;
				},

				build: function() {
					$( '[data-fancybox]' ).fancybox( {
						keyboard : true,
						loop : true,
						toolbar : true,
						infobar : true,
						transitionEffect : 'slide',
						image : {
							preload : 'auto'
						},
						buttons : [
							'slideShow',
							'fullScreen',
							'thumbs',
							'close'
						],
						arrows : true
					} );
				}
			}

		} );
	}
).apply( this, [ window.vnh, jQuery ] );

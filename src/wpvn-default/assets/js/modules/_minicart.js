(
	function( vnh, $ ) {
		vnh = vnh || {};
		$.extend( vnh, {

			miniCart: {

				init: function() {
					this.build();
					return this;
				},

				build: function() {
					var $miniCartTrigger = $( '.minicart__button' ),
					    $miniCart        = $( '.widget_shopping_cart_content' );

					$miniCartTrigger.click( function() {
						$miniCartTrigger.toggleClass( 'active' );
						$miniCart.toggleClass( 'active' );
						return false;
					} );

					$( document ).on( 'click', function( e ) {
						if ( $( e.target ).closest( $miniCart ).length === 0 ) {
							$miniCart.removeClass( 'active' );
						}
						if ( $( e.target ).closest( $miniCartTrigger ).length === 0 ) {
							$miniCartTrigger.removeClass( 'active' );
						}
					} );

				}
			}

		} );
	}
).apply( this, [ window.vnh, jQuery ] );

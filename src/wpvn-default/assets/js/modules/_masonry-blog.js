(
	function( vnh, $ ) {
		vnh = vnh || {};
		$.extend( vnh, {

			MasonryBlog: {

				init: function() {
					this.build();
					return this;
				},

				build: function() {

					var $grid = $( '.grid' ).masonry( {
						itemSelector: '.grid .post',
						columnWidth: '.grid .post',
						percentPosition: true,
						stagger: 30,
						// nicer reveal transition
						visibleStyle: { transform: 'translateY(0)', opacity: 1 },
						hiddenStyle: { transform: 'translateY(100px)', opacity: 0 }
					} );

					// get Masonry instance
					var msnry = $grid.data( 'masonry' );

					// initial items reveal
					$grid.imagesLoaded().progress( function() {
						$grid.masonry( 'layout' );
					} );

					// init Infinte Scroll
					if ( mainScriptParams.nav > 1 ) {
						$grid.infiniteScroll( {
							path: '.nav-previous a',
							append: '.grid .post',
							outlayer: msnry,
							history: false,
							hideNav: '.navigation',
							status: '.page-load-status'
						} );
					}
				}
			}

		} );
	}
).apply( this, [ window.vnh, jQuery ] );

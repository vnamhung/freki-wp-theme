(
	function( vnh, $ ) {
		vnh = vnh || {};
		$.extend( vnh, {

			DropCap: {

				init: function() {
					this.build();
					return this;
				},

				build: function() {
					var dropcapEl = $( '.dropcap' );
					dropcapEl.parent().css( { 'z-index': 0, 'position': 'relative' } );

					dropcapEl.each( function() {
						var $this = $( this );
						var dropcapLetter = $this.text();
						$this.prepend( '<span class="dropcap__duplicate">' + dropcapLetter + '</span>' );
					} );
				}
			}

		} );
	}
).apply( this, [ window.vnh, jQuery ] );

(
	function( vnh, $ ) {
		function vnhOnReady() {

			if ( typeof vnh.DropCap !== 'undefined' ) {
				vnh.DropCap.init();
			}

		}

		$( document ).ready( function() {
			vnhOnReady();
		} );
	}.apply( this, [ window.vnh, jQuery ] )
);

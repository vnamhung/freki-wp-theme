<?php
/**
 * Enqueue scripts for child theme
 */
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style( 'main', esc_url( get_parent_theme_file_uri( 'style.css' ) ) );
	wp_enqueue_style( 'child', get_stylesheet_uri() );
	wp_enqueue_script( 'custom-js', esc_url( get_theme_file_uri( 'assets/js/custom.js' ) ), array( 'jquery' ), null, true );
} );
